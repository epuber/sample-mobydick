
# Sample book - Moby Dick

This is example source code for book Moby Dick to demonstrate tool [Epuber](https://epuber.io).

Specification file for Epuber is `moby_dick.bookspec`.


## Notes

Files with extension `.bade` use syntax of templating language _Bade_. Documentation is not public yet. Syntax is similar to [Jade](http://jade-lang.com).
